﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TaskList
{
    public class TaskListAdapter : BaseAdapter<TaskListModel>
    {
        List<TaskListModel> items;
        Activity _context;
        public TaskListAdapter(Activity context, List<TaskListModel> items) : base()
        {
            this._context = context;
            this.items = items;
        }
        public override long GetItemId(int position)
        {
            return position;
        }
        public override TaskListModel this[int position]
        {
            get { return items[position]; }
        }
        public override int Count
        {
            get { return items.Count; }
        }
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView;
            if (view == null) 
                view = _context.LayoutInflater.Inflate(Resource.Layout.task_item, null);
            view.FindViewById<TextView>(Resource.Id.UserName).Text = items[position].UserName;
            view.FindViewById<TextView>(Resource.Id.UserEmail).Text = items[position].UserEmail;
            view.FindViewById<TextView>(Resource.Id.TaskText).Text = items[position].TaskText;
            view.FindViewById<CheckBox>(Resource.Id.TaskCompletionStatus).Checked = items[position].TaskCompleted;
            view.FindViewById<TextView>(Resource.Id.AdminTaskEdited).Text = items[position].AdminTaskEdited == true ? "Edited by admin" : "";
            return view;
        }

        public void Update(IEnumerable<TaskListModel> newList)
        {
            items.Clear();
            items.AddRange(newList);
            NotifyDataSetChanged();
        }
    }
}