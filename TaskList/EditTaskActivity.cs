﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TaskList
{
    [Activity(Label = "Edit task", Theme = "@style/AppTheme")]
    public class EditTaskActivity: Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_edit_task_item);
            string taskText = Intent.GetStringExtra("task_text");
            bool taskCompleted = Intent.GetBooleanExtra("completed", false);
            EditText taskTextEditText = FindViewById<EditText>(Resource.Id.TaskTextAdminEditText);
            taskTextEditText.Text = taskText;
            CheckBox taskCompletedCheckbox = FindViewById<CheckBox>(Resource.Id.TaskCompletionAdminCheckBox);
            taskCompletedCheckbox.Checked = taskCompleted;

            BindEvents();
        }

        private void BindEvents()
        {
            Button cancelEditBtn = FindViewById<Button>(Resource.Id.CancelEditButton);
            cancelEditBtn.Click += CancelEditBtn_Click; ;

            Button submitEditBtn = FindViewById<Button>(Resource.Id.SubmitEditButton);
            submitEditBtn.Click += SubmitEditBtn_Click; ;
        }

        private async void SubmitEditBtn_Click(object sender, EventArgs e)
        {
            EditText taskTextEditText = FindViewById<EditText>(Resource.Id.TaskTextAdminEditText);
            CheckBox taskCompletedCheckbox = FindViewById<CheckBox>(Resource.Id.TaskCompletionAdminCheckBox);
            int taskId = Intent.GetIntExtra("task_id", -1);

            dynamic response = await MainActivity.TaskController.DoEditTask(taskId, taskTextEditText.Text, taskCompletedCheckbox.Checked);
            if (response.status == "error")
            {
                Toast.MakeText(this, $"Token: {response.message.token}", ToastLength.Long).Show();
                return;
            }
            else
            {
                await MainActivity.TaskController.GetTaskListAsync();
                Finish();
            }
        }

        private void CancelEditBtn_Click(object sender, EventArgs e)
        {
            Finish();
        }
    }
}