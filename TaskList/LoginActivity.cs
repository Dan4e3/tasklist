﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Preferences;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TaskList
{
    [Activity(Label = "Login", Theme = "@style/AppTheme")]
    public class LoginActivity: Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_login);
            BindEvents();
        }

        private void BindEvents()
        {
            Button cancelBtn = FindViewById<Button>(Resource.Id.CancelLoginButton);
            cancelBtn.Click += CancelBtn_Click; ;

            Button submitBtn = FindViewById<Button>(Resource.Id.SubmitLoginButton);
            submitBtn.Click += SubmitBtn_Click;
        }

        private async void SubmitBtn_Click(object sender, EventArgs e)
        {
            EditText usernameEditText = FindViewById<EditText>(Resource.Id.UserNameLoginEditText);
            EditText passwordEditText = FindViewById<EditText>(Resource.Id.PasswordEditText);

            dynamic response = await MainActivity.TaskController.DoLogin(usernameEditText.Text, passwordEditText.Text);
            if (response.status == "error")
            {
                Toast.MakeText(this, $"Username: {response.message.username}\n" +
                $"Password: {response.message.password}\n", ToastLength.Long).Show();
                return;
            }
            else
            {
                Toast.MakeText(this, $"Successfully logged in!", ToastLength.Long).Show();
                ISharedPreferencesEditor editor = MainActivity.Preferences.Edit();
                editor.PutString("auth_token", MainActivity.TaskController.AuthToken);
                editor.Apply();
                Finish();
            }
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Finish();
        }
    }
}