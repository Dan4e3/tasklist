﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TaskList
{
    public class TaskListModel
    {
        string _taskCodeStatus;

        public int Id { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string TaskText { get; set; }
        public bool TaskCompleted { get; private set; }
        public bool AdminTaskEdited { get; private set; }
        public string TaskCodeStatus
        {
            get { return _taskCodeStatus; }
            set
            {
                _taskCodeStatus = value;
                switch (_taskCodeStatus)
                {
                    case "0":
                        TaskCompleted = false;
                        AdminTaskEdited = false;
                        break;
                    case "1":
                        TaskCompleted = false;
                        AdminTaskEdited = true;
                        break;
                    case "10":
                        TaskCompleted = true;
                        AdminTaskEdited = false;
                        break;
                    case "11":
                        TaskCompleted = true;
                        AdminTaskEdited = true;
                        break;
                }
            }
        }
    }
}