﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using System.Net.Http;
using System;
using System.Collections.Generic;
using Android.Content;
using Android.Preferences;

namespace TaskList
{
    [Activity(Label = "Task List to Do", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        public static TaskListConroller TaskController;
        public static ISharedPreferences Preferences;
        protected async override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activity_main);
            TaskController = new TaskListConroller(this);
            BindUIEvents();

            Preferences = PreferenceManager.GetDefaultSharedPreferences(this);
            if (Preferences.Contains("auth_token"))
                TaskController.AuthToken = Preferences.GetString("auth_token", null);


            List<TaskListModel> tasks = await TaskController.GetTaskListAsync();
            if (tasks == null)
                Toast.MakeText(this, "Couldn't load the data", ToastLength.Long).Show();
            ListView taskListView = FindViewById<ListView>(Resource.Id.TaskListView);
            taskListView.Adapter = TaskController.Adapter;
        }

        private void TaskController_MaxPageUpdated(object sender, EventArgs e)
        {
            LinearLayout paginatorLayout = FindViewById<LinearLayout>(Resource.Id.PaginatorLayout);
            if (TaskController.MaxPage <= 1)
            {
                paginatorLayout.Visibility = Android.Views.ViewStates.Invisible;
                return;
            }
            else
                paginatorLayout.Visibility = Android.Views.ViewStates.Visible;

            Button prevButton = FindViewById<Button>(Resource.Id.PreviousPageButton);
            Button nextButton = FindViewById<Button>(Resource.Id.NextPageButton);

            if (TaskController.CurrentPage == 1)
                prevButton.Enabled = false;
            else
                prevButton.Enabled = true;

            if (TaskController.CurrentPage == TaskController.MaxPage)
                nextButton.Enabled = false;
            else
                nextButton.Enabled = true;

        }

        private void BindUIEvents()
        {
            ListView taskListView = FindViewById<ListView>(Resource.Id.TaskListView);
            taskListView.ItemClick += TaskListView_ItemClick;
            Button addNewTaskBtn = FindViewById<Button>(Resource.Id.AddNewTaskButton);
            addNewTaskBtn.Click += AddNewTaskBtn_Click;
            Button prevButton = FindViewById<Button>(Resource.Id.PreviousPageButton);
            prevButton.Click += PrevButton_Click;
            Button nextButton = FindViewById<Button>(Resource.Id.NextPageButton);
            nextButton.Click += NextButton_Click;
            Spinner sortBySpinner = FindViewById<Spinner>(Resource.Id.SortBySpinner);
            sortBySpinner.ItemSelected += SortBySpinner_ItemSelected;
            Spinner sortOrderSpinner = FindViewById<Spinner>(Resource.Id.SortOrderSpinner);
            sortOrderSpinner.ItemSelected += SortOrderSpinner_ItemSelected;
            Button loginButton = FindViewById<Button>(Resource.Id.LoginButton);
            loginButton.Click += LoginButton_Click;
            TaskController.MaxPageUpdated += TaskController_MaxPageUpdated;
            TaskController.UserAuthorized += TaskController_UserAuthorized;
        }

        private void TaskController_UserAuthorized(object sender, EventArgs e)
        {
            TextView authorizedTextView = FindViewById<TextView>(Resource.Id.UserAuthorizedTextView);
            Button loginButton = FindViewById<Button>(Resource.Id.LoginButton);
            
            if (TaskController.AuthToken != null)
            {
                authorizedTextView.Text = "Logged in!";
                loginButton.Text = "Logout";
                loginButton.Click -= LoginButton_Click;
                loginButton.Click += Logout_Click;
            }
            else
            {
                authorizedTextView.Text = "You are not logged in!";
                loginButton.Text = "Login";
                loginButton.Click -= Logout_Click;
                loginButton.Click += LoginButton_Click;
            }
        }

        private async void SortOrderSpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            TaskController.SortDirection = e.Parent.GetItemAtPosition(e.Position).ToString();
            await TaskController.GetTaskListAsync();
        }

        private void TaskListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            if (TaskController.AuthToken == null)
            {
                Toast.MakeText(this, "Authorize in order to edit tasks", ToastLength.Short).Show();
                return;
            }
                
            Intent editIntent = new Intent(this, typeof(EditTaskActivity));
            TaskListModel clickedItem = TaskController.Tasks[e.Position];
            editIntent.PutExtra("task_text", clickedItem.TaskText);
            editIntent.PutExtra("completed", clickedItem.TaskCompleted);
            editIntent.PutExtra("task_id", clickedItem.Id);
            StartActivity(editIntent);
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(LoginActivity));
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            TaskController.DoLogout();
            Toast.MakeText(this, "Logged out!", ToastLength.Short).Show();
            if (Preferences.Contains("auth_token"))
            {
                var editor = Preferences.Edit();
                editor.Remove("auth_token");
                editor.Apply();
            } 
        }

        private async void SortBySpinner_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            TaskController.SortField = e.Parent.GetItemAtPosition(e.Position).ToString();
            await TaskController.GetTaskListAsync();
        }

        private async void NextButton_Click(object sender, EventArgs e)
        {
            if (TaskController.CurrentPage == TaskController.MaxPage)
                return;

            TaskController.CurrentPage++;
            FindViewById<TextView>(Resource.Id.CurrentPageTextView).Text = TaskController.CurrentPage.ToString();
            await TaskController.GetTaskListAsync();
        }

        private async void PrevButton_Click(object sender, EventArgs e)
        {
            if (TaskController.CurrentPage == 1)
                return;
            TaskController.CurrentPage--;
            FindViewById<TextView>(Resource.Id.CurrentPageTextView).Text = TaskController.CurrentPage.ToString();
            await TaskController.GetTaskListAsync();
        }

        private void AddNewTaskBtn_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(AddNewTaskActivity));
        }
    }
}