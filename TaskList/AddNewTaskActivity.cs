﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace TaskList
{
    [Activity(Label = "Add New Task", Theme = "@style/AppTheme")]
    public class AddNewTaskActivity: Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.activiy_add_task_item);
            BindEvents();
        }

        private void BindEvents()
        {
            Button cancelAddingTaskBtn = FindViewById<Button>(Resource.Id.CancelAddTaskButton);
            cancelAddingTaskBtn.Click += CancelAddingTaskBtn_Click;

            Button submitAddNewTaskBtn = FindViewById<Button>(Resource.Id.SubmitAddTaskButton);
            submitAddNewTaskBtn.Click += AddNewTaskBtn_Click;
        }

        private async void AddNewTaskBtn_Click(object sender, EventArgs e)
        {
            EditText usernameEditText = FindViewById<EditText>(Resource.Id.UserNameEditText);
            EditText emailEditText = FindViewById<EditText>(Resource.Id.EmailEditText);
            EditText taskText = FindViewById<EditText>(Resource.Id.TaskTextEditText);
            dynamic res = await MainActivity.TaskController.CreateNewTaskItem(
                usernameEditText.Text, emailEditText.Text, taskText.Text
                );

            if (res.status == "error")
                Toast.MakeText(this, $"Username: {res.message.username}\n" +
                    $"Email: {res.message.email}\n" +
                    $"Task text: {res.message.text}", ToastLength.Long).Show();
            else
            {
                Toast.MakeText(this, "Task added!", ToastLength.Long).Show();
                await MainActivity.TaskController.GetTaskListAsync();
                Finish();
            }
                
        }

        private void CancelAddingTaskBtn_Click(object sender, EventArgs e)
        {
            Finish();
        }
    }
}