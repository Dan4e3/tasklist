﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;

namespace TaskList
{
    public class TaskListConroller
    {
        private const string DEV_NAME = "DaniilChuprinko";
        private HttpClient _httpClient;
        private int _maxPage = 1;
        private string _authToken;
        private HttpClient HttpClient
        {
            get { return _httpClient; }
            set
            {
                _httpClient = value;
                _httpClient.BaseAddress = new Uri(@"https://uxcandy.com/~shapoval/test-task-backend/v2/");
            }
        }
        public List<TaskListModel> Tasks { get; private set; } = new List<TaskListModel>();
        public TaskListAdapter Adapter { get; private set; }
        public int CurrentPage { get; set; } = 1;
        public int MaxPage
        {
            get { return _maxPage; }
            set
            {
                _maxPage = value;
                MaxPageUpdated?.Invoke(this, new EventArgs());
            }
        }
        public string SortDirection { get; set; }
        public string SortField { get; set; }
        public string AuthToken
        {
            get { return _authToken; }
            set
            {
                _authToken = value;
                UserAuthorized?.Invoke(this, new EventArgs());
            }
        }
        public event EventHandler MaxPageUpdated;
        public event EventHandler UserAuthorized;

        public TaskListConroller(Activity context)
        {
            HttpClient = new HttpClient();
            Adapter = new TaskListAdapter(context, Tasks);
        }

        public async Task<List<TaskListModel>> GetTaskListAsync()
        {
            Dictionary<string, string> paramsDict = new Dictionary<string, string>()
            {
                {"developer", DEV_NAME },
                {"sort_field", SortField },
                {"sort_direction", SortDirection },
                {"page", CurrentPage.ToString() }
            };

            string query = string.Join('&', paramsDict.Select(x => {
                if (x.Value != null)
                    return $"{x.Key}={x.Value}";
                else
                    return null;
            }).Where(x => x != null));
            
            HttpResponseMessage response = await HttpClient.GetAsync($"?{query}");
            string responseString = await response.Content.ReadAsStringAsync();
            dynamic results = JsonConvert.DeserializeObject<dynamic>(responseString);
            if (results.status != "ok")
                return null;

            List<TaskListModel> resList = new List<TaskListModel>();
            foreach (var task in results.message.tasks)
                resList.Add(BuildTaskFromJson(task));
            MaxPage = (int)Math.Ceiling((double)results.message.total_task_count  / 3d);

            Tasks = resList;
            RefreshTaskList();

            return resList;
        }

        private void RefreshTaskList()
        {
            Adapter.Update(Tasks);
        }

        private TaskListModel BuildTaskFromJson(dynamic task)
        {
            return new TaskListModel()
            {
                Id = task.id,
                UserName = task.username,
                UserEmail = task.email,
                TaskText = task.text,
                TaskCodeStatus = task.status
            };
        }

        public async Task<dynamic> CreateNewTaskItem(string username, string email, string taskText)
        {
            HttpClient = new HttpClient();
            MultipartFormDataContent content = new MultipartFormDataContent();
            content.Add(new StringContent(username), "username");
            content.Add(new StringContent(email), "email");
            content.Add(new StringContent(taskText), "text");

            HttpResponseMessage response = await HttpClient.PostAsync($"create?developer={DEV_NAME}", content);

            string responseString = await response.Content.ReadAsStringAsync();
            dynamic results = JsonConvert.DeserializeObject<dynamic>(responseString);
            if (results.status == "ok")
            {
                Tasks.Add(BuildTaskFromJson(results.message));
                MaxPage = (int)Math.Ceiling(Tasks.Count / 3.0);
                RefreshTaskList();
            }

            return results;
        }
        
        public async Task<dynamic> DoLogin(string username, string password)
        {
            HttpClient = new HttpClient();
            MultipartFormDataContent content = new MultipartFormDataContent();
            content.Add(new StringContent(username), "username");
            content.Add(new StringContent(password), "password");

            HttpResponseMessage response = await HttpClient.PostAsync($"login?developer={DEV_NAME}", content);
            string responseString = await response.Content.ReadAsStringAsync();
            dynamic results = JsonConvert.DeserializeObject<dynamic>(responseString);
            if (results.status == "ok")
                AuthToken = results.message.token;
            
            return results;
        }

        public void DoLogout()
        {
            AuthToken = null;
        }

        public async Task<dynamic> DoEditTask(int taskId, string taskText, bool status)
        {
            int calculatedStatus = -1;
            TaskListModel modelToEdit = Tasks.First(x => taskId == x.Id);
            if (!status && modelToEdit.TaskText == taskText)
                calculatedStatus = 0;
            else if (!status && modelToEdit.TaskText != taskText)
                calculatedStatus = 1;
            else if (status && modelToEdit.TaskText == taskText)
                calculatedStatus = 10;
            else if (status && modelToEdit.TaskText != taskText)
                calculatedStatus = 11;

            HttpClient = new HttpClient();
            MultipartFormDataContent content = new MultipartFormDataContent();
            content.Add(new StringContent(AuthToken), "token");
            content.Add(new StringContent(taskText), "text");
            content.Add(new StringContent(calculatedStatus.ToString()), "status");

            HttpResponseMessage response = await HttpClient.PostAsync($"edit/{taskId.ToString()}?developer={DEV_NAME}", content);
            string responseString = await response.Content.ReadAsStringAsync();
            dynamic results = JsonConvert.DeserializeObject<dynamic>(responseString);
            if (results.status == "error")
                DoLogout();

            return results;
        }
    }
}